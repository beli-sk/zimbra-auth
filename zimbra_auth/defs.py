__version__ = '0.0.3'
app_name = 'zimbra_auth'
app_description = 'Zimbra Authenticator'
app_name_desc = '{} - {}'.format(app_name, app_description)
