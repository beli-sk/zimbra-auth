import struct


class BaseBuffer(object):

    def __init__(self, on_complete=None):
        self.on_complete = on_complete
        
    def complete(self):
        # called when buffer is complete
        if self.on_complete is not None:
            self.on_complete(self)


class DataBuffer(BaseBuffer):

    @property
    def length(self):
        if isinstance(self._length, DataBuffer):
            return self._length.value()
        else:
            return self._length

    @length.setter
    def length(self, value):
        self._length = value

    def __init__(self, length, *args, **kwargs):
        self.buff = ''
        self.length = length
        super(DataBuffer, self).__init__(*args, **kwargs)

    def update(self, datafile):
        remain = self.length - len(self.buff)
        if remain > 0:
            data = datafile.read(self.length - len(self.buff))
            self.buff = self.buff + data
            assert len(self.buff) <= self.length, 'buffer overflow'
            if self.is_complete():
                self.complete()
            return len(data)
        else:
            raise ValueError('tried to update a complete buffer')

    def is_complete(self):
        return len(self.buff) == self.length

    def value(self):
        return self.buff


class StringBuffer(DataBuffer):
    pass


class IntegerBuffer(DataBuffer):

    def __init__(self, fmt, *args, **kwargs):
        self.fmt = fmt
        length = struct.calcsize(fmt)
        super(IntegerBuffer, self).__init__(length, *args, **kwargs)

    def value(self, seq=False):
        ret = struct.unpack(self.fmt, self.buff)
        if not seq and len(ret) == 1:
            ret = ret[0]
        return ret


class StructureBuffer(BaseBuffer):

    def __init__(self, fields, *args, **kwargs):
        self.fields = fields
        self.pos = 0
        super(StructureBuffer, self).__init__(*args, **kwargs)

    def update(self, datafile):
        bytes_total = 0
        if self.is_complete():
            raise ValueError('tried to update a complete buffer')
        while not self.is_complete():
            bytes_read = self.fields[self.pos].update(datafile)
            bytes_total += bytes_read
            if self.fields[self.pos].is_complete():
                self.pos += 1
            else:
                # no more data to read
                break
        else:
            self.complete()
        return bytes_total

    def is_complete(self):
        return self.pos >= len(self.fields) - 1 and self.fields[-1].is_complete()

    def value(self):
        return [x.value() for x in self.fields]


class PrefixedStringBuffer(StructureBuffer):

    def __init__(self, fmt, max_length=1024):
        super(PrefixedStringBuffer, self).__init__(fields=[])
        ib = IntegerBuffer(fmt=fmt, on_complete=self.validate)
        sb = StringBuffer(length=ib)
        self.fields.append(ib)
        self.fields.append(sb)
        self.max_length = max_length

    def validate(self, buff):
        if buff.value() > self.max_length:
            raise ValueError('max_length exceeded')

    def value(self):
        return self.fields[1].value()

