#!/usr/bin/env python

# behave more like Python 3
from __future__ import print_function, division, absolute_import

import os
import sys
import ldap
import struct
import socket
import logging
import argparse
from pprint import pprint
from getpass import getpass
from StringIO import StringIO

from .defs import __version__, app_name, app_description, app_name_desc
from .databuffers import StructureBuffer, PrefixedStringBuffer


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('zimbra_auth')


class ZimbraAuth(object):

    def __init__(self, uris):
        self.uris = uris

    def listen(self, socket_path, mode='0700'):
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.sock.bind(socket_path)
        os.chmod(socket_path, int(mode, base=8))
        try:
            self.sock.listen(10)
            while True:
                connection, client_address = self.sock.accept()
                self._handle_connection(connection, client_address)
        finally:
            try:
                self.sock.close()
            except:
                pass
            if os.path.exists(socket_path):
                os.unlink(socket_path)

    def authenticate(self, login, password, realm=''):
        dn = self._construct_dn(login, realm)
        for uri in self.uris:
            try:
                if self._ldap_auth(uri, dn, password):
                    logger.info('LDAP URI %s: successful authentication of %s@%s', uri, login, realm)
                    return True
            except:
                logger.error('LDAP URI %s error authentication %s@%s: ', uri, login, realm, exc_info=True)
        logger.info('Failed authentication of %s@%s', login, realm)
        return False

    def _handle_connection(self, connection, client_address):
        input_buffer = StructureBuffer([
            PrefixedStringBuffer('!H'), # 0: login
            PrefixedStringBuffer('!H'), # 1: password
            PrefixedStringBuffer('!H'), # 2: service
            PrefixedStringBuffer('!H'), # 3: realm
            ])
        while not input_buffer.is_complete():
            data = connection.recv(1024)
            dataio = StringIO(data)
            input_buffer.update(dataio)
        login, password, service, realm = input_buffer.value()
        if self.authenticate(login, password, realm):
            connection.sendall(struct.pack('!H', 2) + 'OK')
        else:
            connection.sendall(struct.pack('!H', 2) + 'NO')


    def _ldap_auth(self, uri, dn, password):
        l = ldap.initialize(uri)
        l.set_option(ldap.OPT_X_TLS,ldap.OPT_X_TLS_DEMAND)
        l.start_tls_s()
        try:
            l.simple_bind_s(dn, password)
            return True
        except ldap.INVALID_CREDENTIALS:
            return False

    def _construct_dn(self, login, realm):
        dcs = ('dc=%s' % ldap.dn.escape_dn_chars(x) for x in realm.split('.'))
        return 'uid=%s,ou=people,%s' % (ldap.dn.escape_dn_chars(login), ','.join(dcs))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--uri', nargs='+', required=True)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-i', '--interactive', action='store_true')
    group.add_argument('-l', '--listen', metavar='SOCKET')
    parser.add_argument('-m', '--mode', default='0700',
            help='socket permissions (default: %(default)s')
    args = parser.parse_args()
    if args.interactive:
        login = raw_input('Login: ')
        password = getpass('Password: ')
        login, realm = login.split('@')
        za = ZimbraAuth(args.uri)
        if(za.authenticate(login, password, realm)):
            print('OK')
        else:
            print('NO')
    else:
        za = ZimbraAuth(args.uri)
        za.listen(args.listen, mode=args.mode)

if __name__ == '__main__':
    main()
