#!/usr/bin/env python
import unittest
import struct
from StringIO import StringIO

from zimbra_auth.databuffers import *


class IntegerBufferTest(unittest.TestCase):

    def test_longer_data(self):
        data = struct.pack('!HH', 65535, 46)
        dataio = StringIO(data)
        ib1 = IntegerBuffer(fmt='!H') # network byte-order, 16-bit unsigned
        ib1.update(dataio)
        self.assertEqual(ib1.value(), 65535)
        ib2 = IntegerBuffer(fmt='!H')
        ib2.update(dataio)
        self.assertEqual(ib2.value(), 46)

    def test_shorter_data(self):
        data = struct.pack('!L', 0xf0000000)
        data = data + 'abc'
        ib1 = IntegerBuffer(fmt='!L') # network byte-order, 32-bit unsigned
        dataio = StringIO(data[0])
        ib1.update(dataio)
        self.assertEqual(ib1.is_complete(), False)
        dataio = StringIO(data[1:3])
        ib1.update(dataio)
        self.assertEqual(ib1.is_complete(), False)
        dataio = StringIO(data[3:])
        ib1.update(dataio)
        self.assertEqual(ib1.is_complete(), True)
        self.assertEqual(ib1.value(), 0xf0000000)
        # the extra characters preserved
        self.assertEqual(dataio.read(), 'abc')


class StringBufferTest(unittest.TestCase):

    def test(self):
        data = 'abcdefghij'
        sb = StringBuffer(length=7)
        dataio = StringIO(data[0:3])
        sb.update(dataio)
        self.assertFalse(sb.is_complete())
        dataio = StringIO(data[3:5])
        sb.update(dataio)
        self.assertFalse(sb.is_complete())
        dataio = StringIO(data[5:])
        sb.update(dataio)
        self.assertTrue(sb.is_complete())
        self.assertEqual(sb.value(), data[:7])
        # the extra characters preserved
        self.assertEqual(dataio.read(), data[7:])


class StructureBufferTest(unittest.TestCase):

    def test(self):
        data = struct.pack('!H', 4) + 'abcd' + struct.pack('!H', 3) + 'efghij'
        fields = []
        ib = IntegerBuffer('!H')
        fields.append(ib)
        fields.append(StringBuffer(length=ib))
        ib = IntegerBuffer('!H')
        fields.append(ib)
        fields.append(StringBuffer(length=ib))
        stb = StructureBuffer(fields=fields)
        dataio = StringIO(data[:4])
        stb.update(dataio)
        self.assertFalse(stb.is_complete())
        dataio = StringIO(data[4:])
        stb.update(dataio)
        self.assertTrue(stb.is_complete())
        self.assertEqual(stb.value(), [4, 'abcd', 3, 'efg'])
        # the extra characters preserved
        self.assertEqual(dataio.read(), 'hij')


class PrefixedStringBufferTest(unittest.TestCase):

    def test(self):
        data = struct.pack('!H', 4) + 'abcdefg'
        psb = PrefixedStringBuffer('!H')
        dataio = StringIO(data[0:3])
        psb.update(dataio)
        self.assertFalse(psb.is_complete())
        dataio = StringIO(data[3:])
        psb.update(dataio)
        self.assertTrue(psb.is_complete())
        self.assertEqual(psb.value(), 'abcd')
        # the extra characters preserved
        self.assertEqual(dataio.read(), 'efg')


if __name__ == '__main__':
    unittest.main()

