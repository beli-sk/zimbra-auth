Zimbra Authenticator
====================

Saslauthd replacement to authenticate against one or more Zimbra servers using
LDAP protocol.

Development status: **Alpha**


Locations
---------

The `project page`_ is hosted on Bitbucket.

If you've never contributed to a project on Bitbucket, there is
a `quick start guide`_.

If you find something wrong or know of a missing feature, please
`create an issue`_ on the project page. If you find that inconvenient or have
some security concerns, you could also drop me a line at <devel@beli.sk>.

.. _project page:         https://bitbucket.org/beli-sk/zimbra-auth
.. _quick start guide:    https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo%2C+Compare+Code%2C+and+Create+a+Pull+Request
.. _create an issue:      https://bitbucket.org/beli-sk/zimbra-auth/issues


License
-------

Copyright 2016 Michal Belica <devel@beli.sk>

::

    Zimbra-auth is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    Zimbra-auth is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with the software.  If not, see <http://www.gnu.org/licenses/>.

A copy of the license can be found in the ``LICENSE`` file in the
distribution.

