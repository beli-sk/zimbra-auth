#!/usr/bin/env python
from setuptools import setup, find_packages
from codecs import open # To use a consistent encoding
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

# get version number
defs = {}
with open(path.join(here, 'zimbra_auth/defs.py')) as f:
    exec(f.read(), defs)

setup(
    name='zimbra-auth',
    version=defs['__version__'],
    description=defs['app_description'],
    long_description=long_description,
    url='https://bitbucket.org/beli-sk/zimbra-auth',
    author="Michal Belica",
    author_email="devel@beli.sk",
    license="GPL-3",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2',
        ],
    keywords=['zimbra', 'ldap', 'sasl'],
    zip_safe=True,
    install_requires=[
        'python-ldap',
        ],
    packages=['zimbra_auth'],
    entry_points={
        'console_scripts': [
            'zimbra_auth = zimbra_auth:main',
            ],
        },
    )

